
import 'dart:math';

void main(){
  List<String> appList = ['FNB Banking 2012', 'SnapScan 2013', 'Live Inspect 2014', 'WumDrop 2015', 'Domestly 2016', 'Shyft 2017', 'Khula ecosystem 2018', 'Naked Insurance 2019', 'Easy Equities 2019', 'Ambani Africa 2021'];

  //A)
  appList.sort();
  print(appList);

  //B)
  print('The Winning App of 2017 and 2018 was: ' + appList[7] + ' and ' + appList[4]); //Note this prints from the sorted list therefore positions are based on the sorted list you will see in the output and not the list above

  //C)
  var len = appList.length;
  print('The total number of apps: ' + len.toString());
  

}