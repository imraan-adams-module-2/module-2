class App{
  String? appName;
  String? category;
  String? developer;
  String? year;

  App({this.appName, this.category, this.developer, this.year});

  convertToCaps( name){
    return name.toUpperCase();

  }

}

void main(){
  var app1 = App(appName: 'Ambani Africa', category:'Best Gaming Solution, Best Educational Solution, Best South African Solution',developer: 'Mukundi Lambani', year: '2021' );
  //a)
  print(app1.appName);
  print(app1.category);
  print(app1.developer);
  print(app1.year);
  //b)
  var name = app1.appName;
  print(app1.convertToCaps(name));

}